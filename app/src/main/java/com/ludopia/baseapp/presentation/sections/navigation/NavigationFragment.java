package com.ludopia.baseapp.presentation.sections.navigation;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.util.Log;
import android.view.View;

import com.ludopia.baseapp.R;
import com.ludopia.baseapp.data.modules.navigation.NavigationContract;
import com.ludopia.baseapp.data.modules.navigation.NavigationRouter;
import com.ludopia.baseapp.presentation.base.BaseFragment;

import butterknife.BindView;

/**
 * Created by manuelmunoz on 10/30/16.
 */

public class NavigationFragment extends BaseFragment<NavigationContract.ActionListener>
    implements NavigationContract.View {

    @BindView(R.id.navigation_view) NavigationView mNavigationView;

    @Override public void onViewCreated(View view, Bundle onSavedInstanceState){
        super.onViewCreated(view, onSavedInstanceState);

        setNavigationClickListener();
    }

    @Override protected int getLayoutId() {
        return R.layout.fragment_navigation;
    }

    @Override
    protected NavigationContract.ActionListener setUpActionListener() {

        NavigationRouter wireframe = new NavigationRouter(getApplication());
        return wireframe.buildModule(this);
    }

    void setNavigationClickListener(){
        mNavigationView.setNavigationItemSelectedListener(item -> {

            switch (item.getItemId()){
                case R.id.item_search:
                    mActionListener.presentSearch();
                    return true;
                case R.id.item_favorites:
                    mActionListener.presentFavorites();
                    return true;
            }
            return false;
        });

    }
}
