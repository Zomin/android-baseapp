package com.ludopia.baseapp.presentation.sections.movie.detail;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ludopia.baseapp.R;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.data.modules.favorites.add.AddFavoriteContract;
import com.ludopia.baseapp.data.modules.favorites.add.AddFavoriteRouter;
import com.ludopia.baseapp.data.modules.favorites.add.AddFavoriteWireframe;
import com.ludopia.baseapp.presentation.base.BaseFragmentActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class MovieDetailActivity extends BaseFragmentActivity implements ActionBarImageCallback,
        AddFavoriteContract.View{

    @BindView(R.id.iv_toolbar_image) ImageView mToolbarImageView;
    @BindView(R.id.fab) FloatingActionButton mFloatingActionButton;

    private Movie mMovie;
    private AddFavoriteContract.ActionListener mActionListener;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        configureActivity();

        AddFavoriteRouter wireframe = new AddFavoriteRouter(getBaseApp());
        mActionListener = wireframe.buildModule(this);
    }

    @Override protected void onDestroy() {
        mActionListener.detach();
        super.onDestroy();
    }

    @OnClick(R.id.fab) public void favoriteMovie(){

        if(mMovie == null) return;

        mActionListener.favoriteMovie(mMovie);
    }

    /** Implementation ActionBarImageCallback **/

    @Override public void onMovieLoaded(Movie movie) {
        mMovie = movie;
        updateFavoriteIcon(mMovie.isFavorite());
        Glide.with(this).load(movie.getCoverUrl()).into(mToolbarImageView);
    }

    //Call this from fragment to hide fab
    @Override public void hideFab(){
        mFloatingActionButton.setVisibility(View.INVISIBLE);
    }

    @Override public void showFab(){
        mFloatingActionButton.setVisibility(View.VISIBLE);
    }

    /** End **/

    /** Implementation FavoritesView **/

    @Override public void updateFavoriteIcon(boolean isFavorite) {

        mFloatingActionButton.setImageResource(isFavorite ? R.drawable.ic_favorite_black_24dp :
            R.drawable.ic_favorite_border_black_24dp);
    }

    @Override public void showError(String error) {

    }

    /** End **/
}
