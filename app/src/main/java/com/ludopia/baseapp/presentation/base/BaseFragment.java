package com.ludopia.baseapp.presentation.base;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ludopia.baseapp.R;
import com.ludopia.baseapp.data.base.BaseActionListener;
import com.ludopia.baseapp.data.base.BasePresenter;
import com.ludopia.baseapp.data.base.BaseView;

import butterknife.ButterKnife;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Base Fragment class for all fragments in project {@link Fragment} subclass.
 */
public abstract class BaseFragment<A extends BaseActionListener> extends Fragment implements BaseView{

    protected A mActionListener;

    protected MaterialDialog mMaterialDialog;
    protected MaterialDialog mMaterialProgress;

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);

        ButterKnife.bind(this, view);

        mActionListener = setUpActionListener();

        return view;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        if(mActionListener != null) mActionListener.detach();
    }

    protected void showToast(String title) {
        Toast.makeText(getActivity(), title, Toast.LENGTH_LONG)
                .show();
    }

    protected void showDialog(String title, String content){

        mMaterialDialog = new MaterialDialog.Builder(getContext())
                .title(title)
                .content(content)
                .positiveText(R.string.dialog_positive_text)
                .onPositive((MaterialDialog dialog, DialogAction action) ->
                    mMaterialDialog.dismiss()
                )
                .show();
    }

    protected void showLoaderIndeterminate(String title, String content){

        mMaterialProgress = new MaterialDialog.Builder(getContext())
                .title(title)
                .content(content)
                .progress(true, 0)
                .show();
    }

    @Nullable protected BaseApplication getApplication(){

        return (BaseApplication)getActivity().getApplication();
    }

    protected abstract int getLayoutId();
    protected abstract A setUpActionListener();
}
