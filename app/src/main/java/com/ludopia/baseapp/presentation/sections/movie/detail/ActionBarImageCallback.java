package com.ludopia.baseapp.presentation.sections.movie.detail;

import com.ludopia.baseapp.data.models.Movie;

/**
 * Created by manuelmunoz on 10/30/16.
 */

public interface ActionBarImageCallback {
    void onMovieLoaded(Movie movie);
    void hideFab();
    void showFab();
}
