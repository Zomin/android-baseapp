package com.ludopia.baseapp.presentation.base;

import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.ludopia.baseapp.R;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BaseFragmentActivity extends AppCompatActivity {

    public interface Behaviour {
        String FRAGMENT_CLASS_KEY = "fragment_class_key";
        String BUNDLE_FOR_FRAGMENT = "bundle_for_fragment";
        boolean SHOW_BACK_AS_DEFAULT = true;
        String SHOW_BACK_KEY = "show_back_key";
        String TITLE_KEY = "title_key";
        boolean SHOW_TOOLBAR_AS_DEFAULT = true;
        String SHOW_TOOLBAR = "show_toolbar";
    }

    /***
     * BasePresenterFragment can implement this interface to be notified when user performs a back action.
     */
    public interface BackButtonListener {
        /***
         * @return true if activity must handle back action, as removing itself from the stack
         */
        boolean onBackPressed();
    }

    @Nullable @BindView(R.id.app_bar) protected AppBarLayout appBar;
    @Nullable @BindView(R.id.toolbar) protected Toolbar toolbar;
    protected String mAppName;

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override public void setTitle(CharSequence title) {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) actionBar.setTitle(title);
    }

    @Override public void onBackPressed() {
        BaseFragment fragment = getCurrentPresenterFragment();
        BackButtonListener listener = fragment != null && fragment instanceof BackButtonListener ?
                (BackButtonListener) fragment : null;

        if (listener == null) {
            super.onBackPressed();
            return;
        }

        if (listener.onBackPressed()) super.onBackPressed();
    }

    public BaseApplication getBaseApp() {
        return ((BaseApplication)getApplication());
    }

    public BaseFragment getCurrentPresenterFragment() {
        return (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.fl_fragment);
    }

    /**
     * This method should be called after setContentView in an Activity
     */
    protected void configureActivity(){

        ButterKnife.bind(this);

        mAppName = getString(R.string.app_name);

        configureToolbar(toolbar, appBar);
        configureFragment();
    }

    /**
     * Finds fragment class in bundle iwth key Behaviour.FRAGMENT_CLASS_KEY
     * and commit's it into fl_fragment layout.
     */
    private void configureFragment() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null || bundle.getSerializable(Behaviour.FRAGMENT_CLASS_KEY) == null) {

            Log.w(BaseFragment.class.getSimpleName(), "When using "+BaseFragmentActivity.class.getSimpleName()
                + " you should supply a fragment which extends from " +BaseFragment.class.getSimpleName()
                + " by extras");
            return;
        }

        Serializable serializable = bundle.getSerializable(Behaviour.FRAGMENT_CLASS_KEY);
        Class<BaseFragment> clazz = (Class<BaseFragment>) serializable;

        BaseFragment basePresenterFragment = replaceFragment(clazz);
        Bundle bundleFragment = bundle.getBundle(Behaviour.BUNDLE_FOR_FRAGMENT);
        basePresenterFragment.setArguments(bundleFragment);
    }

    public  <T extends BaseFragment> BaseFragment replaceFragment(Class<T> clazz) {
        try {
            BaseFragment fragment = clazz.newInstance();
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_fragment, fragment).commit();
            return fragment;
        } catch (InstantiationException e) {
            throw new IllegalStateException(e.getMessage());
        } catch (IllegalAccessException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    private void configureToolbar(Toolbar toolbar, @Nullable AppBarLayout appBarLayout) {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        boolean showToolbar = Behaviour.SHOW_TOOLBAR_AS_DEFAULT;

        if (actionBar != null) {
            Bundle bundle = getIntent().getExtras();

            if (bundle != null) {
                boolean showBackKey = bundle.getBoolean(Behaviour.SHOW_BACK_KEY, Behaviour.SHOW_BACK_AS_DEFAULT);
                showToolbar = bundle.getBoolean(Behaviour.SHOW_TOOLBAR, showToolbar);
                actionBar.setDisplayHomeAsUpEnabled(showBackKey);
                String title = bundle.getString(Behaviour.TITLE_KEY);
                actionBar.setTitle(title);
            } else {
                actionBar.setDisplayHomeAsUpEnabled(Behaviour.SHOW_BACK_AS_DEFAULT);
                actionBar.setTitle(mAppName);
            }
        }

        setStatusBarColor();

        if (appBarLayout != null)
            appBarLayout.setVisibility(showToolbar ? View.VISIBLE : View.GONE);
    }

    private void setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
    }
}
