package com.ludopia.baseapp.presentation.sections.movie.detail;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.ludopia.baseapp.R;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.data.modules.movie.detail.MovieDetailContract;
import com.ludopia.baseapp.data.modules.movie.detail.MovieDetailRouter;
import com.ludopia.baseapp.presentation.base.BaseFragment;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieDetailFragment extends BaseFragment<MovieDetailContract.ActionListener>
    implements MovieDetailContract.View{

    @BindView(R.id.tv_title) TextView mTitleTextView;
    @BindView(R.id.tv_rating) TextView mRatingTextView;
    @BindView(R.id.tv_year) TextView mYearTextView;
    @BindView(R.id.tv_directors) TextView mDirectorsTextView;
    @BindView(R.id.tv_writters) TextView mWrittersTextView;
    @BindView(R.id.tv_actors) TextView mActorsTextView;
    @BindView(R.id.tv_plot) TextView mPlotTextView;

    public static String MOVIE_DETAIL_TITLE = "movie_detail_title";

    private ActionBarImageCallback mToolbarCallback;

    public MovieDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        if(context instanceof ActionBarImageCallback){
            mToolbarCallback = (ActionBarImageCallback)context;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle onSavedInstanceState){
        super.onViewCreated(view, onSavedInstanceState);


        Bundle bundle = getArguments();
        if(bundle != null){
            String title = bundle.getString(MOVIE_DETAIL_TITLE, "NOTHING");
            mActionListener.requestDetailForMovieWithTitle(title);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_movie_detail;
    }

    @Override
    protected MovieDetailContract.ActionListener setUpActionListener() {

        MovieDetailRouter wireframe = new MovieDetailRouter(getApplication());
        return wireframe.buildModule(this);
    }

    /** Implements MovieDetailView **/

    @Override
    public void showMovie(Movie movie) {

        mToolbarCallback.onMovieLoaded(movie);

        mTitleTextView.setText(movie.getTitle());
        mYearTextView.setText(movie.getYear());
        mRatingTextView.setText(movie.getRating());
        mDirectorsTextView.setText(movie.getDirector());
        mWrittersTextView.setText(movie.getWritters());
        mActorsTextView.setText(movie.getActors());
        mPlotTextView.setText(movie.getPlot());
    }

    @Override
    public void showError(String error) {
        showDialog(getString(R.string.error), error);
    }

    /** End **/
}
