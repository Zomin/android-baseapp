package com.ludopia.baseapp.presentation.sections.home;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.ludopia.baseapp.R;
import com.ludopia.baseapp.data.modules.navigation.NavigationRouter;
import com.ludopia.baseapp.presentation.base.BaseFragmentActivity;
import com.ludopia.baseapp.presentation.sections.movies.favorites.FavoriteMoviesFragment;
import com.ludopia.baseapp.presentation.sections.movies.search.SearchMoviesFragment;
import com.ludopia.baseapp.presentation.sections.navigation.NavigationFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseFragmentActivity implements NavigationRouter.Listener {

    @Nullable
    @BindView(R.id.app_bar) protected AppBarLayout mAppBar;
    @BindView(R.id.drawer_layout) protected DrawerLayout mDrawerLayout;
    @Nullable @BindView(R.id.toolbar) protected Toolbar mToolbar;
    @BindView(R.id.fl_navigation) protected FrameLayout mNavigationLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_fragment);

        ButterKnife.bind(this);

        String title = getString(R.string.app_name);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();

        setUpToggleListener();
        if(actionBar != null){

            actionBar.setTitle(title);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
//            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
        }
        setStatusBarColor();

        configureFragments();

        mDrawerLayout.closeDrawer(mNavigationLayout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);  // OPEN DRAWER
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void configureFragments(){

        Fragment fragment = new SearchMoviesFragment();
        Fragment navigationFragment = new NavigationFragment();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fl_navigation, navigationFragment)
                .replace(R.id.fl_fragment, fragment)
                .commit();
    }

    void setUpToggleListener(){

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout
                , R.string.drawer_open, R.string.drawer_close){

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerSlide(View drawerView, float offset){
                super.onDrawerSlide(drawerView, offset);
            }
        };
        mDrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    //TODO: Refactor this into an Utils
    private void setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
    }

    /** Implementation Navigation Listener **/

    @Override public void onFragmentCommit() {
        mDrawerLayout.closeDrawer(mNavigationLayout);
    }

    /** End **/
}
