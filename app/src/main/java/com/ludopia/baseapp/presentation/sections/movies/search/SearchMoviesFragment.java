package com.ludopia.baseapp.presentation.sections.movies.search;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ludopia.baseapp.R;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.data.modules.search.movies.SearchMovieContract;
import com.ludopia.baseapp.data.modules.search.movies.SearchMoviesRouter;
import com.ludopia.baseapp.presentation.base.BaseFragment;
import com.ludopia.baseapp.presentation.base.BaseRecyclerAdapter;

import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchMoviesFragment extends BaseFragment<SearchMovieContract.ActionListener> implements SearchMovieContract.View {

    @BindView(R.id.rv_movies_list) RecyclerView mMoviesList;

//    private OkRecyclerViewAdapter<Movie, MovieViewGroup> mMovieListAdapter;
    private BaseRecyclerAdapter<Movie, MovieViewHolder> mMovieListAdapter;

    public SearchMoviesFragment() {
        // Required empty public constructor
    }

    @Override public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override public void onViewCreated(View view, Bundle onSavedInstanceState){
        super.onViewCreated(view, onSavedInstanceState);

        setUpMoviesList();
    }

    @Override  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView)MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint(getString(R.string.query_hint_search_movies));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                showLoaderIndeterminate(getString(R.string.loading), getString(R.string.default_loading_content));
                mActionListener.searchForMoviesWithTitle(query);
                searchView.clearFocus();
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {

                mMovieListAdapter.clear();
                return true;
            }
        });
    }

    @Override protected int getLayoutId(){
        return R.layout.fragment_movies;
    }

    @Override protected SearchMovieContract.ActionListener setUpActionListener() {

        SearchMoviesRouter wireframe = new SearchMoviesRouter(getApplication());
        return wireframe.buildModule(this);
    }

    private void setUpMoviesList(){

        mMoviesList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mMovieListAdapter = new BaseRecyclerAdapter<Movie, MovieViewHolder>() {
            @Override
            public MovieViewHolder createViewHolder(ViewGroup parent, LayoutInflater layoutInflater) {
                View view = layoutInflater.inflate(R.layout.item_movie, parent, false);
                return new MovieViewHolder(view);
            }
        };

        mMovieListAdapter.setOnClickListener(
                (Movie movie, MovieViewHolder movieViewGroup, int position) ->
                        mActionListener.onMovieClicked(movie)
        );

        mMoviesList.setAdapter(mMovieListAdapter);
    }

    /** Implementation SearchMovieContract.View **/

    @Override public void showMovies(List<Movie> movies) {

        if(mMaterialProgress != null) mMaterialProgress.dismiss();

        if(movies != null) {
            mMovieListAdapter.setItems(movies);
        }

    }

    @Override public void showError(String error){

        String errorTitle = getString(R.string.error).toUpperCase();
        showDialog(errorTitle, error);
    }

    /** End **/
}
