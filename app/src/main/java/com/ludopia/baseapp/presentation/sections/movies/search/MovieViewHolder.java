package com.ludopia.baseapp.presentation.sections.movies.search;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ludopia.baseapp.R;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.presentation.base.BaseRecyclerViewHolder;

import butterknife.BindView;

/**
 * Created by manuelm on 11/24/16.
 */

public class MovieViewHolder extends BaseRecyclerViewHolder<Movie> {

    @BindView(R.id.tv_movie_title)
    TextView mMovieTitleTextView;
    @BindView(R.id.iv_movie_poster)
    ImageView mPosterImageView;
    @BindView(R.id.iv_favorite)
    ImageView mFavoriteImageView;

    public MovieViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void configure(Movie item) {

        mMovieTitleTextView.setText(item.getTitle());
        Glide.with(itemView.getContext()).load(item.getCoverUrl()).into(mPosterImageView);

        mFavoriteImageView.setVisibility(View.VISIBLE);
        if(item.isFavorite()){
            mFavoriteImageView.setImageResource(R.drawable.ic_favorite_black_24dp);
        }
        else mFavoriteImageView.setVisibility(View.INVISIBLE);
    }
}