package com.ludopia.baseapp.presentation.base;

import android.app.Application;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import io.realm.Realm;

/**
 * Created by manuelmunoz on 10/28/16.
 */

public class BaseApplication extends Application {

    @Override public void onCreate() {
        super.onCreate();
        ApplicationManager.getInstance().registerActivityLifeCycle(this);
        registerRealm();
    }

    void registerRealm(){
        Realm.init(this);
    }

    @Nullable
    public AppCompatActivity getLiveActivity(){
        return (AppCompatActivity) ApplicationManager.getInstance().getLiveActivity();
    }
}
