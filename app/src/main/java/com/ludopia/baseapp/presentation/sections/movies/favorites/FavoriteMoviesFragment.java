package com.ludopia.baseapp.presentation.sections.movies.favorites;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ludopia.baseapp.R;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.data.modules.favorites.getall.GetAllFavoritesContract;
import com.ludopia.baseapp.data.modules.favorites.getall.GetAllFavoritesRouter;
import com.ludopia.baseapp.presentation.base.BaseFragment;
import com.ludopia.baseapp.presentation.base.BaseRecyclerAdapter;
import com.ludopia.baseapp.presentation.sections.movies.search.MovieViewHolder;

import java.util.List;

import butterknife.BindView;

/**
 * Created by manuelm on 11/28/16.
 */

public class FavoriteMoviesFragment extends BaseFragment<GetAllFavoritesContract.ActionListener>
    implements GetAllFavoritesContract.View{

    @BindView(R.id.rv_movies_list)
    RecyclerView mMoviesList;
    BaseRecyclerAdapter<Movie, MovieViewHolder> mAdapter;

    @Override public void onViewCreated(View view, Bundle onSavedInstanceState){
        super.onViewCreated(view, onSavedInstanceState);

        setUpRecyclerView();
        mActionListener.requestFavoriteMovies();
    }

    private void setUpRecyclerView() {

        mMoviesList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mAdapter = new BaseRecyclerAdapter<Movie, MovieViewHolder>() {
            @Override
            public MovieViewHolder createViewHolder(ViewGroup parent, LayoutInflater layoutInflater) {
                View view = layoutInflater.inflate(R.layout.item_movie, parent, false);
                return new MovieViewHolder(view);
            }
        };
        mMoviesList.setAdapter(mAdapter);
    }

    @Override protected int getLayoutId() {
        return R.layout.fragment_movies;
    }

    @Override protected GetAllFavoritesContract.ActionListener setUpActionListener() {

        GetAllFavoritesRouter wireframe = new GetAllFavoritesRouter(getApplication());
        return wireframe.buildModule(this);
    }

    /** Implementation GetFavorites View **/

    @Override public void showFavoriteMovies(List<Movie> movies) {
        mAdapter.setItems(movies);
    }

    @Override public void showError(String error) {
        showDialog(getString(R.string.error), error);
    }

    /** End **/
}
