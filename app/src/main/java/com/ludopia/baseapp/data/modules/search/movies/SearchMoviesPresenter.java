package com.ludopia.baseapp.data.modules.search.movies;

import com.ludopia.baseapp.data.base.BasePresenter;
import com.ludopia.baseapp.data.models.Movie;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by manuelmunoz on 10/28/16.
 */

public class SearchMoviesPresenter extends BasePresenter<SearchMovieContract.View, SearchMoviesWireframe>
        implements SearchMovieContract.ActionListener{

    SearchMoviesInteractorProvider mSearchMoviesInteractor;

    public SearchMoviesPresenter(SearchMovieContract.View view, SearchMoviesRouter router,
                                 SearchMoviesInteractorProvider searchMoviesInteractor) {
        super(view, router);
        mSearchMoviesInteractor = searchMoviesInteractor;
    }

    /** Implementation MoviesActionListener **/

    @Override public void searchForMoviesWithTitle(String title){

        wrapSubscription(mSearchMoviesInteractor.searchMoviesByQuery(title)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(error -> mView.showError(error.getLocalizedMessage()))
                .onErrorResumeNext(throwable -> Observable.empty())
                .subscribe(movies -> mView.showMovies(movies)));
    }

    @Override public void onMovieClicked(Movie movie){
        mRouter.presentMovieDetail(movie);
    }

    /** End **/
}
