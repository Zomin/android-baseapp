package com.ludopia.baseapp.data.modules.favorites.add;

import com.ludopia.baseapp.data.base.BaseDataManager;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.data.services.MovieService;
import com.ludopia.baseapp.data.services.MovieServiceDBImpl;

import io.realm.Realm;
import io.realm.RealmQuery;
import rx.Observable;

/**
 * Created by manuelm on 11/7/16.
 */

public class AddFavoriteDataManager extends BaseDataManager<MovieService> implements AddFavoriteDataManagerProvider {

    private MovieService dbMovieService;

    public AddFavoriteDataManager(MovieService service) {
        super(service);

        dbMovieService = new MovieServiceDBImpl();
        mDisposableManager.addToDisposables(dbMovieService);
    }

    /** Implementation AddFavoriteDataManager Provider **/

    @Override public Observable<Movie> markMovieAsFavorite(Movie movie) {

        return dbMovieService.markMovieAsFavorite(movie);
    }

    /** End **/
}
