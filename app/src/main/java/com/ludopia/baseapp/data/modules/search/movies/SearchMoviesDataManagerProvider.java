package com.ludopia.baseapp.data.modules.search.movies;

import com.ludopia.baseapp.data.base.BaseDataManagerProvider;
import com.ludopia.baseapp.data.models.Movie;

import java.util.List;

import rx.Observable;

/**
 * Created by manuelm on 11/3/16.
 */

public interface SearchMoviesDataManagerProvider extends BaseDataManagerProvider{

    Observable<List<Movie>> searchMoviesByQuery(String query);

}
