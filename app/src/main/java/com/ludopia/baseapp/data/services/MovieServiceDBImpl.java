package com.ludopia.baseapp.data.services;

import com.ludopia.baseapp.data.base.BaseService;
import com.ludopia.baseapp.data.helpers.Disposable;
import com.ludopia.baseapp.data.models.Movie;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by manuelm on 11/28/16.
 */

public class MovieServiceDBImpl implements MovieService, Disposable {

    private Realm mRealm;

    public MovieServiceDBImpl(){
        mRealm = Realm.getDefaultInstance();
    }

    /** Implementation MovieService **/

    @Override public Observable<List<Movie>> allMovies() {

        return mRealm.where(Movie.class).findAll().asObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .map(ArrayList::new);
    }

    @Override public Observable<List<Movie>> searchMoviesByQuery(String query) {
        return Observable.empty();
    }

    @Override public Observable<Movie> detailForMovieWithTitle(String title) {
        return Observable.empty();
    }

    @Override public Observable<Movie> markMovieAsFavorite(Movie movie) {

        Movie query =  mRealm.where(Movie.class)
                .contains(Movie.TITLE_KEY, movie.getTitle())
                .findFirst();

        if(query == null){
            movie.setFavorite(true);
            mRealm.executeTransaction(realm -> realm.insert(movie));
            return Observable.just(movie);
        }

        query.setFavorite(!query.isFavorite());
        return query.asObservable();
    }

    /** End **/

    /** Implementation Disposable **/

    @Override public void dispose() {
        mRealm.close();
    }

    /** End **/
}
