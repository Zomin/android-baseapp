package com.ludopia.baseapp.data.modules.navigation;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.ludopia.baseapp.R;
import com.ludopia.baseapp.data.base.BaseRouting;
import com.ludopia.baseapp.presentation.base.BaseApplication;
import com.ludopia.baseapp.presentation.sections.movies.favorites.FavoriteMoviesFragment;
import com.ludopia.baseapp.presentation.sections.movies.search.SearchMoviesFragment;

/**
 * Created by manuelm on 11/4/16.
 */

public class NavigationRouter extends BaseRouting<NavigationContract.View, NavigationContract.ActionListener>
        implements NavigationWireframe {

    public interface Listener {
        void onFragmentCommit();
    }

    public NavigationRouter(BaseApplication application) {
        super(application);
    }

    @Override public NavigationContract.ActionListener buildModule(NavigationContract.View view) {

        return new NavigationPresenter(view, this);
    }

    private void replaceMainLayoutWithFragment(Fragment fragment){

        /**
         * I am assuming live activity is an instance of BaseFragmentActivity
         * So it contains a FrameLayout called fl_fragment
         * {@link com.ludopia.baseapp.presentation.base.BaseFragmentActivity}
         **/

        AppCompatActivity liveActivity = mApplication.getLiveActivity();

        liveActivity
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fl_fragment, fragment)
                .commit();

        if(liveActivity instanceof Listener) ((Listener) liveActivity).onFragmentCommit();
    }

    /** Implementation NavigationWireframe **/

    @Override public void presentSearchMovies() {

        Fragment searchFragment = new SearchMoviesFragment();
        replaceMainLayoutWithFragment(searchFragment);
    }

    @Override public void presentFavorites() {

        Fragment favoriteFragment = new FavoriteMoviesFragment();
        replaceMainLayoutWithFragment(favoriteFragment);
    }

    /** End **/
}
