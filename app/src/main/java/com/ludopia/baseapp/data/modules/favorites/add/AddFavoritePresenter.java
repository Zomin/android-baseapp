package com.ludopia.baseapp.data.modules.favorites.add;

import com.ludopia.baseapp.data.base.BasePresenter;
import com.ludopia.baseapp.data.models.Movie;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by manuelm on 11/7/16.
 */

public class AddFavoritePresenter extends BasePresenter<AddFavoriteContract.View, AddFavoriteWireframe> implements AddFavoriteContract.ActionListener{

    AddFavoriteInteractorProvider mAddFavoriteInteractor;

    public AddFavoritePresenter(AddFavoriteContract.View view, AddFavoriteWireframe router, AddFavoriteInteractorProvider addFavoriteInteractor) {
        super(view, router);

        mAddFavoriteInteractor = addFavoriteInteractor;
    }

    /** Implementation AddFavoriteContract ActionListener **/

    @Override public void favoriteMovie(Movie movie) {

        wrapSubscription(mAddFavoriteInteractor.markMovieAsFavorite(movie)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(t -> mView.showError(t.getMessage()))
                .onErrorResumeNext(throwable -> Observable.empty())
                .subscribe(movie1 -> mView.updateFavoriteIcon(movie1.isFavorite())));
    }

    /** End **/
}
