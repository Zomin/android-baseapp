package com.ludopia.baseapp.data.modules.navigation;

import com.ludopia.baseapp.data.base.BaseWireframe;

/**
 * Created by manuelm on 11/4/16.
 */

public interface NavigationWireframe extends BaseWireframe {

    void presentSearchMovies();
    void presentFavorites();
}
