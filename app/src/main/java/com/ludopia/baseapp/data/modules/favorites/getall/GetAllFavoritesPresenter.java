package com.ludopia.baseapp.data.modules.favorites.getall;

import com.ludopia.baseapp.data.base.BasePresenter;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by manuelm on 11/28/16.
 */

public class GetAllFavoritesPresenter extends BasePresenter<GetAllFavoritesContract.View, GetAllFavoritesWireframe> implements
        GetAllFavoritesContract.ActionListener{

    GetAllFavoritesInteractorProvider mGetFavoritesInteractor;

    public GetAllFavoritesPresenter(GetAllFavoritesContract.View view, GetAllFavoritesWireframe router,
                                    GetAllFavoritesInteractorProvider getFavoritesInteractor) {
        super(view, router);

        mGetFavoritesInteractor = getFavoritesInteractor;
    }

    /** Implementation GetAllFavoritesContract ActionListener **/
    @Override public void requestFavoriteMovies() {

        wrapSubscription(mGetFavoritesInteractor.requestFavoriteMovies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(error -> mView.showError(error.getLocalizedMessage()))
                .onErrorResumeNext(throwable -> Observable.empty())
                .subscribe(movies -> mView.showFavoriteMovies(movies)));
    }

    /** End **/

}
