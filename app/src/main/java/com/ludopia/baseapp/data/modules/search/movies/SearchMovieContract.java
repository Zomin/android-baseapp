package com.ludopia.baseapp.data.modules.search.movies;

import com.ludopia.baseapp.data.base.BaseActionListener;
import com.ludopia.baseapp.data.base.BaseView;
import com.ludopia.baseapp.data.models.Movie;

import java.util.List;

/**
 * Created by manuelm on 11/3/16.
 */

public interface SearchMovieContract {

    interface View extends BaseView {

        void showMovies(List<Movie> movies);
        void showError(String error);

    }

    interface ActionListener extends BaseActionListener {

        void searchForMoviesWithTitle(String title);
        void onMovieClicked(Movie movie);
    }
}
