package com.ludopia.baseapp.data.base;

/**
 * Created by manuelmunoz on 10/28/16.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.ludopia.baseapp.presentation.base.BaseApplication;

/**
 * Base interface for Routers or Wireframes.
 * This class is incharge of linking all components from the module
 */
public abstract class BaseRouting<V extends BaseView, A extends BaseActionListener> {

    //TODO: Maybe make all Routing classes parcelable?

    protected BaseApplication mApplication;

    public BaseRouting(BaseApplication application){
        mApplication = application;
    }

    public abstract A buildModule(V view);
}
