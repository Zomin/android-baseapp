package com.ludopia.baseapp.data.modules.favorites.getall;

import com.ludopia.baseapp.data.base.BaseDataManager;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.data.services.MovieService;

import java.util.List;

import rx.Observable;

/**
 * Created by manuelm on 11/28/16.
 */

public class GetAllFavoritesDataManager extends BaseDataManager<MovieService> implements GetAllFavoritesDataManagerProvider {

    public GetAllFavoritesDataManager(MovieService service) {
        super(service);
    }

    /** Implementation GetAllFavoritesDataManager Provider **/

    @Override public Observable<List<Movie>> getAllFavoriteMovies() {

        return mService.allMovies();
    }

    /** End **/
}
