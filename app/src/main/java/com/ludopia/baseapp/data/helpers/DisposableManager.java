package com.ludopia.baseapp.data.helpers;

import java.util.ArrayList;

/**
 * Created by manuelm on 11/3/16.
 */

public class DisposableManager {

    ArrayList<Disposable> mDisposables;

    public DisposableManager(){

        mDisposables = new ArrayList<>();
    }

    /**
     * Call this method on every presenter/interactor/dataManager/Service
     * to keep track of disposables
     * @param object implements {@link Disposable}
     */
    public void addToDisposables(Object object){

        if(object == null) return;
        if(object instanceof Disposable) mDisposables.add((Disposable)object);
    }

    public void disposeAll(){
        for (Disposable dis: mDisposables) {
            dis.dispose();
        }
    }
}
