package com.ludopia.baseapp.data.base;

/**
 * Created by manuelmunoz on 10/28/16.
 */

/**
 * Base interface for any interface of type view. These interfaces will be implemented for the pertinent
 * screen in the presentation layer
 */
public interface BaseView {
}
