package com.ludopia.baseapp.data.base;

/**
 * Created by manuelmunoz on 10/28/16.
 */

/**
 * Base interface for any interactor. Presenters will call interactor through this interface.
 */
public interface BaseInteractorDataProvider {
}
