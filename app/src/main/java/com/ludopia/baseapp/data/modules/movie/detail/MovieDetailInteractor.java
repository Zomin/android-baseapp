package com.ludopia.baseapp.data.modules.movie.detail;

import com.ludopia.baseapp.data.base.BaseInteractor;
import com.ludopia.baseapp.data.models.Movie;

import rx.Observable;


/**
 * Created by manuelm on 11/4/16.
 */

public class MovieDetailInteractor extends BaseInteractor<MovieDetailDataManagerProvider>
        implements MovieDetailInteractorProvider {

    public MovieDetailInteractor() {
        super();
    }

    /** Implementation MovieDetailInteractorDataProvider **/

    @Override public Observable<Movie> getDetailForMovieWithTitle(String title) {
        return mDataManager.detailForMovieWithTitle(title);
    }

    /** End **/
}
