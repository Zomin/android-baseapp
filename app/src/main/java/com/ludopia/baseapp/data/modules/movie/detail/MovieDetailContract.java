package com.ludopia.baseapp.data.modules.movie.detail;

import com.ludopia.baseapp.data.base.BaseActionListener;
import com.ludopia.baseapp.data.base.BaseView;
import com.ludopia.baseapp.data.models.Movie;

/**
 * Created by manuelm on 11/4/16.
 */

public interface MovieDetailContract {

    interface View extends BaseView {

        void showMovie(Movie movie);
        void showError(String error);
    }

    interface ActionListener extends BaseActionListener {

        void requestDetailForMovieWithTitle(String title);
    }
}
