package com.ludopia.baseapp.data.helpers;

/**
 * Created by manuelm on 10/31/16.
 */

public interface Disposable {

    /**
     * Perform any clean up here
     */
    void dispose();
}
