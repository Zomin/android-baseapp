package com.ludopia.baseapp.data.modules.search.movies;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ludopia.baseapp.R;
import com.ludopia.baseapp.data.models.Movie;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by manuelm on 11/24/16.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    ArrayList<Movie> mMovies;

    public MovieAdapter(){
        mMovies = new ArrayList<>();
    }

    public MovieAdapter(ArrayList<Movie> movies){
        mMovies = movies;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Movie movie = mMovies.get(position);
        holder.titleTextView.setText(movie.getTitle());
        Glide.with(holder.itemView.getContext()).load(movie.getCoverUrl()).into(holder.coverImageView);
    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }

    public void setMovies(ArrayList<Movie> movies){
        mMovies = movies;
        notifyDataSetChanged();
    }

    public void clear(){
        mMovies.clear();
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_movie_title)
        TextView titleTextView;
        @BindView(R.id.iv_movie_poster)
        ImageView coverImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
