package com.ludopia.baseapp.data.models;

/**
 * Created by manuelmunoz on 10/28/16.
 */

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Wrapper for Movies
 */
public class Search {

    @SerializedName("Search")
    ArrayList<Movie> mMovies;

    public ArrayList<Movie> getMovies() {
        return mMovies;
    }

    public void setMovies(ArrayList<Movie> movies) {
        mMovies = movies;
    }
}
