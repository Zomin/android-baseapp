package com.ludopia.baseapp.data.modules.navigation;

import com.ludopia.baseapp.data.base.BaseActionListener;
import com.ludopia.baseapp.data.base.BaseView;

/**
 * Created by manuelm on 11/4/16.
 */

public interface NavigationContract {

    interface View extends BaseView {


    }

    interface ActionListener extends BaseActionListener {

        void presentSearch();
        void presentFavorites();
    }
}
