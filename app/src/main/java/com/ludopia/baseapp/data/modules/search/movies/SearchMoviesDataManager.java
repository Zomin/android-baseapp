package com.ludopia.baseapp.data.modules.search.movies;

import com.ludopia.baseapp.data.base.BaseDataManager;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.data.services.MovieService;
import com.ludopia.baseapp.data.services.MovieServiceDBImpl;

import java.util.List;

import rx.Observable;

/**
 * Created by manuelm on 11/3/16.
 */

public class SearchMoviesDataManager extends BaseDataManager<MovieService> implements SearchMoviesDataManagerProvider {

    private MovieService dbMovieService;

    public SearchMoviesDataManager(MovieService service) {
        super(service);

        dbMovieService = new MovieServiceDBImpl();

        mDisposableManager.addToDisposables(dbMovieService);
    }

    /** Implementation SearchMoviesDataManager **/

    @Override public Observable<List<Movie>> searchMoviesByQuery(String query) {

        return mService.searchMoviesByQuery(query);
    }

    /** End **/
}
