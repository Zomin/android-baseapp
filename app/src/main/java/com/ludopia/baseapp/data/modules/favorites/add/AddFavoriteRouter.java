package com.ludopia.baseapp.data.modules.favorites.add;

import com.ludopia.baseapp.data.base.BaseRouting;
import com.ludopia.baseapp.data.services.MovieServiceImpl;
import com.ludopia.baseapp.presentation.base.BaseApplication;

/**
 * Created by manuelm on 11/21/16.
 */

public class AddFavoriteRouter extends BaseRouting<AddFavoriteContract.View, AddFavoriteContract.ActionListener>
    implements AddFavoriteWireframe {

    public AddFavoriteRouter(BaseApplication application) {
        super(application);
    }

    @Override public AddFavoriteContract.ActionListener buildModule(AddFavoriteContract.View view) {

        AddFavoriteInteractor interactor = new AddFavoriteInteractor();
        AddFavoriteDataManager dataManager = new AddFavoriteDataManager(new MovieServiceImpl());
        AddFavoritePresenter presenter = new AddFavoritePresenter(view, this, interactor);

        interactor.setDataManager(dataManager);

        return presenter;
    }

    /** ImplementationAddFavoriteWireframe **/

    /** End **/
}
