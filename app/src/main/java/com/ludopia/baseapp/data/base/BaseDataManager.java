package com.ludopia.baseapp.data.base;

/**
 * Created by manuelmunoz on 10/28/16.
 */

import com.ludopia.baseapp.data.helpers.Disposable;
import com.ludopia.baseapp.data.helpers.DisposableManager;

/**
 * Base class for data managers. All other data manager interfaces must extend from this one. This class
 * handles retrieving data from data bases or from a web service.
 */
public abstract class BaseDataManager<S extends BaseService> implements Disposable {

    protected S mService;
    protected DisposableManager mDisposableManager;

    public BaseDataManager(S service) {

        mService = service;

        mDisposableManager = new DisposableManager();

        mDisposableManager.addToDisposables(mService);
    }

    /** Implementation Disposable **/

    @Override public void dispose() {
        mDisposableManager.disposeAll();
    }
}
