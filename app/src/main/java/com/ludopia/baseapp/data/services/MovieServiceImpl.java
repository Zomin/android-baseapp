package com.ludopia.baseapp.data.services;

import com.ludopia.baseapp.data.base.BaseService;
import com.ludopia.baseapp.data.helpers.Disposable;
import com.ludopia.baseapp.data.helpers.RetrofitManager;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.data.models.MoviesApi;
import com.ludopia.baseapp.data.models.Search;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by manuelm on 11/3/16.
 */

public class MovieServiceImpl implements MovieService, Disposable {

    /** Implementation MovieService **/

    @Override public Observable<List<Movie>> searchMoviesByQuery(String query) {

        return RetrofitManager.build(MoviesApi.BASE_URL).create(MoviesApi.class)
                .search(query)
                .map(search -> search.getMovies());
//                .enqueue(new Callback<Search>() {
//                    @Override
//                    public void onResponse(Call<Search> call, Response<Search> response) {
//
//                        if(response.isSuccessful()){
//
//                            listener.onSuccess(response.body().getMovies());
//                            return;
//                        }
//                        errorListener.onError(response.errorBody().toString());
//                    }
//
//                    @Override
//                    public void onFailure(Call<Search> call, Throwable t) {
//
//                        errorListener.onError(t.getMessage());
//                    }
//                });

    }

    @Override public Observable<Movie> detailForMovieWithTitle(String title){

        return RetrofitManager.build(MoviesApi.BASE_URL).create(MoviesApi.class)
                .detailByTitle(title);

//                .enqueue(new Callback<Movie>() {
//                    @Override
//                    public void onResponse(Call<Movie> call, Response<Movie> response) {
//
//                        if(response.isSuccessful()){
//                            listener.onSuccess(response.body());
//                            return;
//                        }
//                        failureListener.onError(response.errorBody().toString());
//                    }
//
//                    @Override
//                    public void onFailure(Call<Movie> call, Throwable t) {
//
//                        failureListener.onError(t.getMessage());
//                    }
//                });
    }

    @Override public Observable<Movie> markMovieAsFavorite(Movie movie) {
        return Observable.empty();
    }

    @Override
    public Observable<List<Movie>> allMovies() {
        return Observable.empty();
    }

    /** End **/

    /** Implementation Disposable **/
    @Override
    public void dispose() {

    }

    /** End **/
}
