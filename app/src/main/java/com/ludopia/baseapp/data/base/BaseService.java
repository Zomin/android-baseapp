package com.ludopia.baseapp.data.base;

import rx.Observable;

/**
 * Created by manuelm on 11/3/16.
 */

public interface BaseService {

    //This are separated into two interfaces to use lambda expressions
    interface ListenerSuccess<T>{
//        void onSuccess(T item);
        Observable<T> onSuccess();
    }
    interface ListenerFailure{
        Observable<Throwable> onError();
//        void onError(String error);
    }


}
