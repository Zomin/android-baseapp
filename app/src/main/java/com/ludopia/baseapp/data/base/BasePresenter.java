package com.ludopia.baseapp.data.base;

import com.ludopia.baseapp.data.helpers.DisposableManager;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by manuelmunoz on 10/28/16.
 */

/**
 * Base class for all presenter. This class handles all UI logic, it must simplify data to send to the View
 * so the View doesn't have to make calculations or take decisions.
 * @param <V> Class implementing BaseView interface, normally a Fragment or Activity
 */
public abstract class BasePresenter<V extends BaseView, R extends BaseWireframe> implements BaseActionListener {

    protected V mView;
    protected R mRouter;

    private DisposableManager mDisposableManager;
    private CompositeSubscription mCompositeSubscription;

    public BasePresenter(V view, R router){

        mView = view;
        mRouter = router;

        mDisposableManager = new DisposableManager();
        mCompositeSubscription = new CompositeSubscription();
    }

    protected void wrapSubscription(Subscription subscription){
        mCompositeSubscription.add(subscription);
    }

    /** Base Action Listener Implementation **/

    public void detach(){

        mView = null;
        mDisposableManager.disposeAll();

        if(mCompositeSubscription.isUnsubscribed()) mCompositeSubscription.unsubscribe();
    }

    /** End **/
}
