package com.ludopia.baseapp.data.base;

import com.ludopia.baseapp.data.helpers.Disposable;
import com.ludopia.baseapp.data.helpers.DisposableManager;

/**
 * Created by manuelmunoz on 10/28/16.
 */

/**
 * Base class for all interactors. This class handles business logic or 'UseCases'.
 * @param <D> Class implementing DataManagerProvider interface, normally a data manager.
 */
public abstract class BaseInteractor<D extends BaseDataManagerProvider>
        implements BaseInteractorDataProvider, Disposable {

    protected D mDataManager;

    private DisposableManager mDisposableManager;

    public BaseInteractor(){

        mDisposableManager = new DisposableManager();
    }

    public void setDataManager(D dataManager){
        mDataManager = dataManager;
        mDisposableManager.addToDisposables(mDataManager);
    }

    /** Implementation Disposable **/

    @Override public void dispose() {
        mDisposableManager.disposeAll();
    }

    /** End **/
}
