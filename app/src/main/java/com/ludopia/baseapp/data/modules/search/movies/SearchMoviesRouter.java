package com.ludopia.baseapp.data.modules.search.movies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.ludopia.baseapp.data.base.BaseActionListener;
import com.ludopia.baseapp.data.base.BaseRouting;
import com.ludopia.baseapp.data.base.BaseView;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.data.services.MovieServiceImpl;
import com.ludopia.baseapp.presentation.base.BaseApplication;
import com.ludopia.baseapp.presentation.base.BaseFragment;
import com.ludopia.baseapp.presentation.base.BaseFragmentActivity;
import com.ludopia.baseapp.presentation.sections.movie.detail.MovieDetailActivity;
import com.ludopia.baseapp.presentation.sections.movie.detail.MovieDetailFragment;

/**
 * Created by manuelm on 11/3/16.
 */

public class SearchMoviesRouter extends BaseRouting<SearchMovieContract.View,
        SearchMovieContract.ActionListener> implements SearchMoviesWireframe{

    public SearchMoviesRouter(BaseApplication application) {
        super(application);
    }

    @Override public SearchMovieContract.ActionListener buildModule(SearchMovieContract.View view) {

        SearchMoviesInteractor interactor = new SearchMoviesInteractor();
        SearchMoviesDataManager dataManager = new SearchMoviesDataManager(new MovieServiceImpl());
        SearchMoviesPresenter presenter = new SearchMoviesPresenter(view, this, interactor);

        interactor.setDataManager(dataManager);

        return presenter;
    }

    /** Implementation SearchMoviesWireframe **/

    @Override public void present() {
        //TODO: How to present {@link MovieFragment} inside an activity?
    }

    @Override public void presentMovieDetail(Movie movie) {

        Intent intent = new Intent(mApplication.getLiveActivity(), MovieDetailActivity.class);
        Bundle fragmentBundle = new Bundle();
        fragmentBundle.putString(MovieDetailFragment.MOVIE_DETAIL_TITLE, movie.getTitle());

        intent.putExtra(BaseFragmentActivity.Behaviour.SHOW_BACK_KEY, true);
        intent.putExtra(BaseFragmentActivity.Behaviour.FRAGMENT_CLASS_KEY, MovieDetailFragment.class);
        intent.putExtra(BaseFragmentActivity.Behaviour.BUNDLE_FOR_FRAGMENT, fragmentBundle);
        intent.putExtra(BaseFragmentActivity.Behaviour.SHOW_TOOLBAR, true);
        intent.putExtra(BaseFragmentActivity.Behaviour.TITLE_KEY, movie.getTitle());

        Activity activity = mApplication.getLiveActivity();
        if(activity != null) activity.startActivity(intent);
    }

    /** End **/
}
