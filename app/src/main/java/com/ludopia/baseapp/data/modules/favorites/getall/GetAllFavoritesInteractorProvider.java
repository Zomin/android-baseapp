package com.ludopia.baseapp.data.modules.favorites.getall;

import com.ludopia.baseapp.data.base.BaseInteractorDataProvider;
import com.ludopia.baseapp.data.models.Movie;

import java.util.List;

import rx.Observable;

/**
 * Created by manuelm on 11/28/16.
 */

public interface GetAllFavoritesInteractorProvider extends BaseInteractorDataProvider{

    Observable<List<Movie>> requestFavoriteMovies();
}
