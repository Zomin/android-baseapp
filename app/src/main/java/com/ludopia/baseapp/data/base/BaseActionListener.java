package com.ludopia.baseapp.data.base;

/**
 * Created by manuelmunoz on 10/28/16.
 */

/**
 * Base interface for any presenter. This interface will handle communication with a View
 */
public interface BaseActionListener {

    /**
     * use this method when a View is about to be destroyed
     */
    void detach();
}
