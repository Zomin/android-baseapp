package com.ludopia.baseapp.data.modules.movie.detail;

import com.ludopia.baseapp.data.base.BaseDataManager;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.data.services.MovieService;

import java.util.HashMap;

import io.realm.Realm;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by manuelm on 11/4/16.
 */

public class MovieDetailDataManager extends BaseDataManager<MovieService>
        implements MovieDetailDataManagerProvider {

    private HashMap<String, Movie> mMovieHashMap;
    private Realm mRealm;

    public MovieDetailDataManager(MovieService service) {
        super(service);

        mMovieHashMap = new HashMap<>();
        mRealm = Realm.getDefaultInstance();
    }

    @Override public void dispose(){
        mRealm.close();
        super.dispose();
    }

    private Observable<Movie> searchMovieInDB(String movieTitle){

        Movie query = mRealm.where(Movie.class)
                .contains(Movie.TITLE_KEY, movieTitle)
                .findFirst();

        return query == null ? Observable.just(null) : query.asObservable();
    }

    /** Implementation MovieDetailDataManagerDataProvider **/

    @Override public Observable<Movie> detailForMovieWithTitle(String title) {

        if(mMovieHashMap.containsKey(title)){
//            mDataOutput.receiveMovie(mMovieHashMap.get(title));
            return Observable.just(mMovieHashMap.get(title));
        }

        return mService.detailForMovieWithTitle(title)
                .observeOn(AndroidSchedulers.mainThread())
                .zipWith(searchMovieInDB(title), (movie, movie2) -> {
                    if(movie2 != null) movie.setFavorite(movie2.isFavorite());
                    return movie;
                });
    }

    /** End **/
}
