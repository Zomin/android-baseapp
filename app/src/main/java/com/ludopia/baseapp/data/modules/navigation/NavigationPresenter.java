package com.ludopia.baseapp.data.modules.navigation;

import android.support.design.widget.NavigationView;

import com.ludopia.baseapp.data.base.BaseInteractorDataProvider;
import com.ludopia.baseapp.data.base.BasePresenter;
import com.ludopia.baseapp.data.base.BaseWireframe;

/**
 * Created by manuelm on 11/4/16.
 */

public class NavigationPresenter extends BasePresenter<NavigationContract.View, NavigationWireframe>
        implements NavigationContract.ActionListener {

    public NavigationPresenter(NavigationContract.View view, NavigationWireframe router) {
        super(view, router);
    }

    /** Implementation NavigationContract ActionListener **/

    @Override public void presentSearch() {
        mRouter.presentSearchMovies();
    }

    @Override public void presentFavorites() {
        mRouter.presentFavorites();
    }

    /** End **/
}
