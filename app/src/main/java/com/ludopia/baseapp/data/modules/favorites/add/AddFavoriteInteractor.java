package com.ludopia.baseapp.data.modules.favorites.add;

import com.ludopia.baseapp.data.base.BaseInteractor;
import com.ludopia.baseapp.data.models.Movie;

import rx.Observable;

/**
 * Created by manuelm on 11/7/16.
 */

public class AddFavoriteInteractor extends BaseInteractor<AddFavoriteDataManagerProvider>
        implements AddFavoriteInteractorProvider{

    public AddFavoriteInteractor() {
        super();
    }

    /** Implementation AddFavoriteInteractor DataProvider **/

    @Override public Observable<Movie> markMovieAsFavorite(Movie movie) {

        return mDataManager.markMovieAsFavorite(movie);
    }

    /** End **/
}
