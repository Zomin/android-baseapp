package com.ludopia.baseapp.data.modules.search.movies;

import com.ludopia.baseapp.data.base.BaseInteractor;
import com.ludopia.baseapp.data.models.Movie;

import java.util.List;

import rx.Observable;

/**
 * Created by manuelmunoz on 10/28/16.
 */

public class SearchMoviesInteractor extends BaseInteractor<SearchMoviesDataManagerProvider>
        implements SearchMoviesInteractorProvider{

    public SearchMoviesInteractor(){
        super();
    }

    /** Implementation SearchMoviesDataOutput **/

    @Override public Observable<List<Movie>> searchMoviesByQuery(String query){
        return mDataManager.searchMoviesByQuery(query);
    }

    /** End **/
}
