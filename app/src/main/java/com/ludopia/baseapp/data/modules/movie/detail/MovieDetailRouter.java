package com.ludopia.baseapp.data.modules.movie.detail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.ludopia.baseapp.data.base.BaseRouting;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.data.services.MovieServiceImpl;
import com.ludopia.baseapp.presentation.base.BaseApplication;
import com.ludopia.baseapp.presentation.base.BaseFragmentActivity;
import com.ludopia.baseapp.presentation.sections.movie.detail.MovieDetailActivity;
import com.ludopia.baseapp.presentation.sections.movie.detail.MovieDetailFragment;

/**
 * Created by manuelm on 11/4/16.
 */

public class MovieDetailRouter extends BaseRouting<MovieDetailContract.View, MovieDetailContract.ActionListener>
    implements MovieDetailWireframe {

    public MovieDetailRouter(BaseApplication application) {
        super(application);
    }

    @Override public MovieDetailContract.ActionListener buildModule(MovieDetailContract.View view) {

        MovieDetailInteractor interactor = new MovieDetailInteractor();
        MovieDetailDataManager dataManager = new MovieDetailDataManager(new MovieServiceImpl());
        MovieDetailPresenter presenter = new MovieDetailPresenter(view, this, interactor);

        interactor.setDataManager(dataManager);

        return presenter;
    }

    /** Implementation MovieDetailWireframe **/

    @Override  public void present(Movie movie) {

        Bundle bundle = new Bundle();
        bundle.putBoolean(BaseFragmentActivity.Behaviour.SHOW_BACK_KEY,
                BaseFragmentActivity.Behaviour.SHOW_BACK_AS_DEFAULT);
        bundle.putBoolean(BaseFragmentActivity.Behaviour.SHOW_TOOLBAR,
                BaseFragmentActivity.Behaviour.SHOW_TOOLBAR_AS_DEFAULT);
        bundle.putString(BaseFragmentActivity.Behaviour.TITLE_KEY, movie.getTitle());
        bundle.putSerializable(BaseFragmentActivity.Behaviour.FRAGMENT_CLASS_KEY, MovieDetailFragment.class);

        Bundle fragmentBundle = new Bundle();
        fragmentBundle.putString(MovieDetailFragment.MOVIE_DETAIL_TITLE, movie.getTitle());

        bundle.putBundle(BaseFragmentActivity.Behaviour.BUNDLE_FOR_FRAGMENT, fragmentBundle);

        Activity liveActivity = mApplication.getLiveActivity();
        if(liveActivity != null){
            Intent intent = new Intent(mApplication, MovieDetailActivity.class);
            intent.putExtras(bundle);
            liveActivity.startActivity(intent);
        }
    }

    @Override public void presentMovieSearch() {
        //TODO: Do something? i think this will be empty cause back is managed by home button
    }

    /** End **/
}
