package com.ludopia.baseapp.data.helpers;

import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by manuelmunoz on 10/28/16.
 */

public class RetrofitManager {

    private static Retrofit mRetrofit;

    public static Retrofit build(String baseUrl){
        return build(baseUrl, GsonConverterFactory.create());
    }

    public static Retrofit build(String baseUrl, Converter.Factory factory){

        if(mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(factory)
                    .build();
        }
        return mRetrofit;
    }
}
