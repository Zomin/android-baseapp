package com.ludopia.baseapp.data.modules.favorites.getall;

import com.ludopia.baseapp.data.base.BaseInteractor;
import com.ludopia.baseapp.data.models.Movie;

import java.util.List;

import rx.Observable;

/**
 * Created by manuelm on 11/28/16.
 */

public class GetAllFavoritesInteractor extends BaseInteractor<GetAllFavoritesDataManagerProvider>
        implements GetAllFavoritesInteractorProvider {

    public GetAllFavoritesInteractor() {
        super();
    }

    /** Implementation GetAllFavoritesInteractor Dataprovider **/

    @Override public Observable<List<Movie>> requestFavoriteMovies() {
        return mDataManager.getAllFavoriteMovies();
    }

    /** End **/
}
