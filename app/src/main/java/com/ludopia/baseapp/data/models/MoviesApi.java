package com.ludopia.baseapp.data.models;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by manuelmunoz on 10/28/16.
 */

public interface MoviesApi {

    String BASE_URL = "http://www.omdbapi.com/";

//    @GET("?s")
//    Call<Search> search(@Query("s") String query);

//    @GET("?t")
//    Call<Movie> detailByTitle(@Query("t") String title);

    @GET("?")
    Observable<Search> search(@Query("s") String query);

    @GET("?")
    Observable<Movie> detailByTitle(@Query("t") String title);
}
