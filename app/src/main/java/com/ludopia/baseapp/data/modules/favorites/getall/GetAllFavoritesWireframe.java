package com.ludopia.baseapp.data.modules.favorites.getall;

import com.ludopia.baseapp.data.base.BaseWireframe;

/**
 * Created by manuelm on 11/28/16.
 */

public interface GetAllFavoritesWireframe extends BaseWireframe {
}
