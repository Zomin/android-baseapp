package com.ludopia.baseapp.data.modules.favorites.getall;

import com.ludopia.baseapp.data.base.BaseActionListener;
import com.ludopia.baseapp.data.base.BaseView;
import com.ludopia.baseapp.data.models.Movie;

import java.util.List;

/**
 * Created by manuelm on 11/28/16.
 */

public interface GetAllFavoritesContract {

    interface View extends BaseView {

        void showFavoriteMovies(List<Movie> movies);
        void showError(String error);
    }

    interface ActionListener extends BaseActionListener {

        void requestFavoriteMovies();
    }
}
