package com.ludopia.baseapp.data.modules.movie.detail;

import com.ludopia.baseapp.data.base.BasePresenter;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by manuelm on 11/4/16.
 */

public class MovieDetailPresenter extends BasePresenter<MovieDetailContract.View, MovieDetailWireframe>
        implements MovieDetailContract.ActionListener{

    MovieDetailInteractorProvider mMovieDetailInteractor;

    public MovieDetailPresenter(MovieDetailContract.View view, MovieDetailWireframe router,
                                MovieDetailInteractorProvider movieDetailInteractor) {
        super(view, router);

        mMovieDetailInteractor = movieDetailInteractor;
    }

    /** Implementation MovieDetailContract ActionListener **/

    @Override public void requestDetailForMovieWithTitle(String title) {

        wrapSubscription(mMovieDetailInteractor.getDetailForMovieWithTitle(title)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(t -> mView.showError(t.getMessage()))
                .subscribe(movie -> mView.showMovie(movie)));
    }

    /** End **/
}
