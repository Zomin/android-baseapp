package com.ludopia.baseapp.data.modules.search.movies;

import com.ludopia.baseapp.data.base.BaseWireframe;
import com.ludopia.baseapp.data.models.Movie;

/**
 * Created by manuelm on 11/3/16.
 */

public interface SearchMoviesWireframe extends BaseWireframe {

    void present();
    void presentMovieDetail(Movie movie);
}
