package com.ludopia.baseapp.data.modules.favorites.add;

import com.ludopia.baseapp.data.base.BaseDataManagerProvider;
import com.ludopia.baseapp.data.models.Movie;

import rx.Observable;

/**
 * Created by manuelm on 11/4/16.
 */

public interface AddFavoriteDataManagerProvider extends BaseDataManagerProvider {

    Observable<Movie> markMovieAsFavorite(Movie movie);
}
