package com.ludopia.baseapp.data.modules.movie.detail;

import com.ludopia.baseapp.data.base.BaseWireframe;
import com.ludopia.baseapp.data.models.Movie;

/**
 * Created by manuelm on 11/4/16.
 */

public interface MovieDetailWireframe extends BaseWireframe {

    void present(Movie movie);
    void presentMovieSearch();
}
