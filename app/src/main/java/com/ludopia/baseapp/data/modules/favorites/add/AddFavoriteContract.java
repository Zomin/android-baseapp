package com.ludopia.baseapp.data.modules.favorites.add;

import com.ludopia.baseapp.data.base.BaseActionListener;
import com.ludopia.baseapp.data.base.BaseView;
import com.ludopia.baseapp.data.models.Movie;

/**
 * Created by manuelm on 11/4/16.
 */

public interface AddFavoriteContract {

    interface View extends BaseView {

        void updateFavoriteIcon(boolean isFavorite);
        void showError(String error);
    }

    interface ActionListener extends BaseActionListener {

        void favoriteMovie(Movie movie);
    }
}
