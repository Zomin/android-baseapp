package com.ludopia.baseapp.data.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by manuelmunoz on 10/28/16.
 */

public class Movie extends RealmObject{

    public static final String TITLE_KEY = "mTitle";

    // Primary key should be an id...but this api doesnt give one
    @SerializedName("Title") @PrimaryKey private String mTitle;
    @SerializedName("Poster") private String mCoverUrl;
    @SerializedName("Year") private String mYear;
    @SerializedName("Type") private String mType;
    @SerializedName("Genre") private String mGenre;
    @SerializedName("Director") private String mDirector;
    @SerializedName("Writer") private String mWritters;
    @SerializedName("Actors") private String mActors;
    @SerializedName("Plot") private String mPlot;
    @SerializedName("Language") private String mLanguages;
    @SerializedName("imdbRating") private String mRating;

    private boolean mFavorite;

    public String getCoverUrl() {
        return mCoverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        mCoverUrl = coverUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getYear() {
        return mYear;
    }

    public void setYear(String year) {
        mYear = year;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getGenre() {
        return mGenre;
    }

    public void setGenre(String genre) {
        mGenre = genre;
    }

    public String getDirector() {
        return mDirector;
    }

    public void setDirector(String director) {
        mDirector = director;
    }

    public String getWritters() {
        return mWritters;
    }

    public void setWritters(String writters) {
        mWritters = writters;
    }

    public String getActors() {
        return mActors;
    }

    public void setActors(String actors) {
        mActors = actors;
    }

    public String getPlot() {
        return mPlot;
    }

    public void setPlot(String plot) {
        mPlot = plot;
    }

    public String getLanguages() {
        return mLanguages;
    }

    public void setLanguages(String languages) {
        mLanguages = languages;
    }

    public String getRating() {
        return mRating;
    }

    public void setRating(String rating) {
        mRating = rating;
    }

    public boolean isFavorite() {
        return mFavorite;
    }

    public void setFavorite(boolean mFavorite) {
        this.mFavorite = mFavorite;
    }
}
