package com.ludopia.baseapp.data.services;

import com.ludopia.baseapp.data.base.BaseService;
import com.ludopia.baseapp.data.models.Movie;
import com.ludopia.baseapp.data.models.Search;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by manuelm on 11/3/16.
 */

public interface MovieService extends BaseService {

    Observable<List<Movie>> allMovies();

    Observable<List<Movie>> searchMoviesByQuery(String query);

    Observable<Movie> detailForMovieWithTitle(String title);

    Observable<Movie> markMovieAsFavorite(Movie movie);
}
