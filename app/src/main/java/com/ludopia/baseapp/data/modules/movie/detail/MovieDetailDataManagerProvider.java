package com.ludopia.baseapp.data.modules.movie.detail;

import com.ludopia.baseapp.data.base.BaseDataManagerProvider;
import com.ludopia.baseapp.data.models.Movie;

import rx.Observable;

/**
 * Created by manuelm on 11/4/16.
 */

public interface MovieDetailDataManagerProvider extends BaseDataManagerProvider{

    Observable<Movie> detailForMovieWithTitle(String title);
}
