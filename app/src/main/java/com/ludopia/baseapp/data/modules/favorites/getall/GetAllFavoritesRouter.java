package com.ludopia.baseapp.data.modules.favorites.getall;

import com.ludopia.baseapp.data.base.BaseRouting;
import com.ludopia.baseapp.data.services.MovieServiceDBImpl;
import com.ludopia.baseapp.presentation.base.BaseApplication;

/**
 * Created by manuelm on 11/28/16.
 */

public class GetAllFavoritesRouter extends BaseRouting<GetAllFavoritesContract.View,
        GetAllFavoritesContract.ActionListener> implements GetAllFavoritesWireframe {

    public GetAllFavoritesRouter(BaseApplication application) {
        super(application);
    }

    @Override public GetAllFavoritesContract.ActionListener buildModule(GetAllFavoritesContract.View view) {


        GetAllFavoritesInteractor interactor = new GetAllFavoritesInteractor();
        GetAllFavoritesDataManager dataManager = new GetAllFavoritesDataManager(new MovieServiceDBImpl());
        GetAllFavoritesPresenter presenter = new GetAllFavoritesPresenter(view, this, interactor);

        interactor.setDataManager(dataManager);

        return presenter;
    }
}
